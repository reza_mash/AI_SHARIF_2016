package client;

import client.model.Graph;
import client.model.Node;

/**
 * AI class.
 * You should fill body of the method {@link #doTurn}.
 * Do not change name or modifiers of the methods or fields
 * and do not add constructor for this class.
 * You can add as many methods or fields as you want!
 * Use world parameter to access and modify game's
 * world!
 * See World interface for more details.
 */
public class AI {
	static Node[] all_nodes;
	static Node[] my_nodes;
	static Node[] op_nodes;
	
	static boolean first_turn = true;
	static int id;
	static long total_turn;
	static int turn;
	static long turn_time;
	static long time_pass;
	static Graph game_graph;
	
	static int escape;
	static int node_bonus;
	static int edge_bonus;
	
	static int weak_max;
	static int medium_max;
	
	static double casualty[]={1.0,0.6667,0.3334};

	//first turn, this method is gonna run
	void initialize_data(World world){
		id = world.getMyID();
		game_graph = world.getMap();
		total_turn = world.getTotalTurns();
		turn_time = world.getTurnTimePassed();
		
		escape = world.getEscapeConstant();
		all_nodes = game_graph.getNodes();
		node_bonus = world.getNodeBonusConstant();
		edge_bonus = world.getEdgeBonusConstant();
		
		weak_max = world.getLowArmyBound();
		medium_max = world.getMediumArmyBound();
		
		casualty[1]=world.getMediumCasualtyCoefficient();
		casualty[2]=world.getLowCasualtyCoefficient();
		
		first_turn = false;
	}
	

    public void doTurn(World world) {
        // fill this method, we've presented a stupid AI for example!

		if(first_turn)
			initialize_data(world);
		turn = world.getTurnNumber();
		my_nodes = world.getMyNodes();
		op_nodes = world.getOpponentNodes();

//        for (Node source : my_nodes) {
//            // get neighbours
//            Node[] neighbours = source.getNeighbours();
//            if (neighbours.length > 0) {
//                // select a random neighbour
//                Node destination = neighbours[(int) (neighbours.length * Math.random())];
//                // move half of the node's army to the neighbor node
//                world.moveArmy(source, destination, source.getArmyCount()/2);
//            }
//        }

    }
}
